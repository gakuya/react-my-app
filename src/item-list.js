import React, {useState} from "react";
import {Checkbox, List, ListItem, ListItemText} from "@material-ui/core";

const initialItems = [
    {id: 1, contents: 'やること１', checked: false},
    {id: 2, contents: 'やること２', checked: true},
    {id: 3, contents: 'やること３', checked: false},
    {id: 4, contents: 'やること４', checked: false},
];

const ItemList = () => {
    const [items, setItems]= useState(initialItems);
    const toggleChecked = (item) =>{
        return () => {
            const targetIndex = items.indexOf(item);
            items[targetIndex].checked = ! items[targetIndex].checked;
            setItems([...items]);
        };
    }
    return(
        <List>
            {items.map(item=>(
                <ListItem key={item.id}>
                    <Checkbox
                        checked={item.checked}
                        onChange={toggleChecked(item)} />
                    <ListItemText primary={item.contents}/>
                </ListItem>
            ))}
        </List>
    );
};
export default ItemList;