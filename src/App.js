import React from "react";
import {Toolbar} from "@material-ui/core";
import { BrowserRouter, Route, Link, Switch} from "react-router-dom";

import LoginForm from "./login-form";
import ItemList from "./item-list";
import Home from "./home";

const App =(props) =>{
  return (
      <BrowserRouter>
          <div>
              <Toolbar>
                  <Link to='/'>トップ</Link>
                  <Link to='/login'>ログイン</Link>
                  <Link to='/list'>一覧</Link>
              </Toolbar>
              <Switch>
                  <Route exact path='/' component = {Home}/>
                  <Route path='/login' component = {LoginForm}/>
                  <Route path='/list' component = {ItemList}/>
              </Switch>
          </div>
      </BrowserRouter>
  );
}
export default App;
