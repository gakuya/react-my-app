import React from "react";
import {makeStyles} from "@material-ui/core";
import {Paper, Typography, Button} from "@material-ui/core";
import { useHistory } from 'react-router-dom';

const useStyles = makeStyles({
    container: {height: '100vh'},
    content: {margin: '200px 150px'}
});
const Home = (props) => {
    const classes = useStyles();
    const history = useHistory();
    return (
        <Paper className={classes.container}>
            <div className={classes.content}>
                <Typography variant='button'>やることリストに</Typography>
                <Button variant='outlined' onClick={()=>history.push('/login')}>
                    ログイン
                </Button>
            </div>
        </Paper>
    );
};

export default Home;